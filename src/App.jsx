import Home from "./components/Home";
import Header from "./components/Header";
import "./App.css";
import { Provider } from "react-redux";
import store from "./store";

function App() {
  return (
    <Provider store={store}>
      <Header />
       <Home />
    </Provider>
  );
}

export default App;
