import { Box,MenuItem,InputLabel,FormControl, Select   } from "@mui/material";
import { useDispatch,useSelector } from "react-redux";
import {orderBy} from '../../store/slices/OrderBySlice'

const Dropdown = () => {
  const dispatch = useDispatch()
  

  const handleChange = (event) => {
    dispatch(orderBy(event.target.value));
  };

  const orderByd = useSelector((state)=>state.orderBy)

  return (
    <Box>
      <FormControl
        sx={{ m: 1, minWidth: 300, bgcolor: "#252729", borderRadius: "0.8rem" }}
      >
        <InputLabel id="demo-simple-select-label" sx={{color:'#fff'}}>Order by</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={orderByd}
          label="Age"
          onChange={handleChange}
          sx={{color:'#fff'}}
        > <MenuItem value={""}>None</MenuItem>
          <MenuItem value={"created"}>Date added</MenuItem>
          <MenuItem value={"name"}>Name</MenuItem>
          <MenuItem value={"released"}>Release date</MenuItem>
          <MenuItem value={"added"}>Popularity</MenuItem>
          <MenuItem value={"rating"}>Average Rating</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
};

export default Dropdown;
