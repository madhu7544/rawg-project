import { Routes, Route, Navigate } from "react-router-dom";
import Sidebar from "./Sidebar";
import Mainpage from "./MainPage/index";
import Genres from "./MainPage/Genres";
import Platforms from "./MainPage/Platforms";


const Home = () => {
  return (
    <div className="main-page">
      <Sidebar />
      <Routes>
        <Route path="/" element={<Navigate to="/games" />} />
        <Route path="/games" element={<Mainpage />} />
        <Route path="/games/genres/:id" element={<Genres />} />
        <Route path="/games/platform/:id" element={<Platforms />} />
      </Routes>
    </div>
  );
};

export default Home;
