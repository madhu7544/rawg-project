import { useState } from "react";
import {
  Box,
  Typography,
  Card,
  ListItem,
  CardMedia,
  Divider,
} from "@mui/material";
import { AiFillWindows } from "react-icons/ai";
import {
  BsPlaystation,
  BsXbox,
  BsNintendoSwitch,
  BsAndroid2,
} from "react-icons/bs";
import { FaLinux, FaAngleRight } from "react-icons/fa";
import { MdOutlinePhoneIphone, MdOutlineLaptopMac } from "react-icons/md";

const EachGame = (props) => {
  const [isHovered, setIsHovered] = useState(false);
  const { game } = props;
  const platforms = game.parent_platforms;
  const getPlatform = (slug) => {
    switch (slug) {
      case "pc":
        return <AiFillWindows className="icons" />;
      case "playstation":
        return <BsPlaystation className="icons" />;
      case "xbox":
        return <BsXbox className="icons" />;
      case "mac":
        return <MdOutlineLaptopMac className="icons" />;
      case "android":
        return <BsAndroid2 className="icons" />;
      case "ios":
        return <MdOutlinePhoneIphone className="icons" />;
      case "linux":
        return <FaLinux className="icons" />;
      case "nintendo":
        return <BsNintendoSwitch className="icons" />;
      default:
        return null;
    }
  };

  const genres = game.genres.map((genre) => genre.name);
  const genreNames = genres.join(", ");

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };

  return (
    <ListItem
      sx={{ width: "24%", position: "relative", mb: "2rem",}}
    >
      <Card
        sx={{
          width: isHovered?"92%":"100%",
          bgcolor: "#252729",
          position: isHovered ? "absolute" : "static",
          top: isHovered ? "1%" : 0,
          zIndex: isHovered ? 2 : 0,
        }}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
      >
        <CardMedia
          sx={{ width: "100%", height: 200 }}
          component="img"
          image={game.background_image}
          alt={game.name}
        />
        <Box sx={{ ml: "1rem" }}>
          <Box sx={{ mt: "1rem" }}>
            {platforms?.map((each, index) => (
              <span key={index}>{getPlatform(each.platform.slug)}</span>
            ))}
          </Box>
          <Typography
            variant="h6"
            sx={{ color: "#fff", mt: "1rem", mb: "1rem" }}
          >
            {game.name}
          </Typography>
          <Box
            sx={{
              mr: "1rem",
              display: isHovered ? "block" : "none",
              opacity: isHovered ? 1 : 0,
              transition: "opacity 0.3s ease",
            }}
          >
            <Box sx={{ display: "flex", justifyContent: "space-between" }}>
              <Typography variant="p" sx={{ color: "#5d6063" }}>
                Release date:
              </Typography>
              <Typography
                variant="p"
                sx={{ color: "#fff", fontSize: "0.9rem" }}
              >
                {game.released}
              </Typography>
            </Box>
            <Divider
              style={{ backgroundColor: "#5d6063", marginTop: "0.5rem" }}
            />
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                mt: "1rem",
              }}
            >
              <Typography variant="p" sx={{ color: "#5d6063" }}>
                Genres:
              </Typography>
              <Typography
                variant="p"
                sx={{ color: "#fff", fontSize: "0.9rem" }}
              >
                {genreNames}
              </Typography>
            </Box>
            <Divider
              style={{ backgroundColor: "#5d6063", marginTop: "0.5rem" }}
            />
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                mt: "1rem",
              }}
            >
              <Typography variant="p" sx={{ color: "#5d6063" }}>
                Chart:
              </Typography>
              <Typography
                variant="p"
                sx={{ color: "#fff", fontSize: "0.8rem" }}
              >
                # Top {new Date(game.released).getFullYear()}
              </Typography>
            </Box>
            <Divider
              style={{
                backgroundColor: "#5d6063",
                marginBottom: "1rem",
                marginTop: "0.5rem",
              }}
            />
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                bgcolor: "#5d6063",
                p: "1rem",
                mb: "1rem",
                borderRadius: "0.5rem",
                cursor: "pointer",
              }}
            >
              <Typography variant="p" sx={{ color: "#fff", fontSize: "1rem" }}>
                Show more like this
              </Typography>
              <FaAngleRight style={{ color: "#fff", fontSize: "1.5rem" }} />
            </Box>
          </Box>
        </Box>
      </Card>
    </ListItem>
  );
};

export default EachGame;
