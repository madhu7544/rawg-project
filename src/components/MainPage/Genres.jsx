import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Box, Typography, List, Button } from "@mui/material";
import EachGame from "./EachGame";
import CircularProgress from "@mui/material/CircularProgress";
import { useParams } from "react-router-dom";
import {getByGenre} from "../api";
import { genereData } from "../../store/slices/GenereDataSlice";
import Dropdown from '../DropDown/DropDown'

function CircularIndeterminate() {
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
        bgcolor: "black",
        width: "92%",
      }}
    >
      <CircularProgress />
    </Box>
  );
}

const Genres = () => {
  const {id} = useParams()
  const dispatch = useDispatch();
  const gamesData = useSelector((state) => state.genreData.games);
  const pageNumberId = useSelector((state) => state.genreData.pageNumber);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const handleLoadMore = () => {
    dispatch(getPageNumber(pageNumberId + 1));
  };

  const orderBy = useSelector((state)=>state.orderBy);

  useEffect(() => {
    getByGenre(id,orderBy)
      .then((response) => {
        dispatch(genereData(response.data.results));
        setLoading(false)
      })
      .catch((error) => {
        setError(error.message)
      });
  }, [id,orderBy]);

  if (loading) {
    return <CircularIndeterminate />;
  }

  if (error) {
    return (
      <Box sx={{ bgcolor: "black", width: "82%", color: "#fff" }}>
        <Typography variant="h3" sx={{ fontWeight: "bold", ml: "1.5rem" }}>
          Error: {error}
        </Typography>
      </Box>
    );
  }

  return (
    <Box sx={{ bgcolor: "black", width: "82%" }}>
      <Typography
        variant="h3"
        sx={{ color: "#fff", fontWeight: "bold", ml: "1.5rem" }}
      >
        {id.charAt(0).toUpperCase() + id.slice(1)}
      </Typography>
      <Dropdown/>
      <List sx={{ display: "flex", flexWrap: "wrap" }}>
        {gamesData?.map((each) => (
          <EachGame key={each.id} game={each} />
        ))}
      </List>
      <Button onClick={handleLoadMore} variant="contained">
        Load More
      </Button>
    </Box>
  );
};
export default Genres;
