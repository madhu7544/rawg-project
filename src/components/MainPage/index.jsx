import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getGamesData, getPageNumber } from "../../store/slices/GameSlice";
import { getGames } from "../api";

import { Box, Typography, List, Button } from "@mui/material";
import EachGame from "./EachGame";

import CircularProgress from "@mui/material/CircularProgress";
import Dropdown from '../DropDown/DropDown'

function CircularIndeterminate() {
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
        bgcolor: "black",
        width: "92%",
      }}
    >
      <CircularProgress />
    </Box>
  );
}

const Mainpage = () => {
  const dispatch = useDispatch();
  const gamesData = useSelector((state) => state.game.games);
  const pageNumberId = useSelector((state) => state.game.pageNumber);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const handleLoadMore = () => {
    dispatch(getPageNumber(pageNumberId + 1));
  };

  const orderBy = useSelector((state)=>state.orderBy);

  useEffect(() => {
    getGames(pageNumberId,orderBy)
      .then((response) => {
        dispatch(getGamesData(response.data.results));
        setLoading(false);
      })
      .catch((error) => {
        setError(error.message);
      });
  }, [pageNumberId,orderBy]);

  if (loading) {
    return <CircularIndeterminate />;
  }

  if (error) {
    return (
      <Box sx={{ bgcolor: "black", width: "82%", color: "#fff" }}>
        <Typography variant="h3" sx={{ fontWeight: "bold", ml: "1.5rem" }}>
          Error: {error}
        </Typography>
      </Box>
    );
  }

  return (
    <Box sx={{ bgcolor: "black", width: "82%" }}>
      <Typography
        variant="h3"
        sx={{ color: "#fff", fontWeight: "bold", ml: "1.5rem" }}
      >
        All Games
      </Typography>
      <Dropdown/>
      <List sx={{ display: "flex", flexWrap: "wrap" }}>
        {gamesData?.map((each) => (
          <EachGame key={each.id} game={each} />
        ))}
      </List>
      <Button onClick={handleLoadMore} variant="contained">
        Load More
      </Button>
    </Box>
  );
};
export default Mainpage;
