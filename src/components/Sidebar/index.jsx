import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Box, Typography, List, ListItem, Button } from "@mui/material";
import { AiFillWindows } from "react-icons/ai";
import {
  BsPlaystation,
  BsXbox,
  BsNintendoSwitch,
  BsAndroid2,
} from "react-icons/bs";
import { FaAngleDown, FaAngleUp } from "react-icons/fa";
import { MdOutlinePhoneIphone } from "react-icons/md";
import { getGenres } from "../api";
import { useDispatch, useSelector } from "react-redux";
import { getGenresData } from "../../store/slices/GenereSlice";

const platforms = [
  { id: 1, icon: AiFillWindows, name: "PC" },
  { id: 2, icon: BsPlaystation, name: "PlayStation 4" },
  { id: 3, icon: BsXbox, name: "Xbox One" },
  { id: 7, icon: BsNintendoSwitch, name: "Nintendo Switch" },
  { id: 5, icon: MdOutlinePhoneIphone, name: "iOS" },
  { id: 8, icon: BsAndroid2, name: "Android" },
];

const style = {
  bgcolor: "#545454",
  width: "2rem",
  height: "2rem",
  borderRadius: "0.3rem",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  transition: "background-color 0.3s",
};

const Sidebar = () => {
  const [hoveredIndex, setHoveredIndex] = useState(null);
  const [showAll, setShowAll] = useState(false);
  const [selectedPlatform, setSelectedPlatform] = useState(null);

  const handleShowAll = () => {
    setShowAll(true);
  };

  const handleHide = () => {
    setShowAll(false);
  };

  const dispatch = useDispatch();

  const data = useSelector((state) => {
    return state.genre;
  });

  const pageNumber = useSelector((state) => {
    return state.game.pageNumber;
  });

  const visibleItems = showAll ? data : data.slice(0, 4);

  useEffect(() => {
    getGenres()
      .then((response) => {
        dispatch(getGenresData(response.data.results));
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const getEachPlatform = (platformId) => {
    setSelectedPlatform(platformId);
  };

  const handleMouseEnter = (index) => {
    setHoveredIndex(index);
  };

  const handleMouseLeave = () => {
    setHoveredIndex(null);
  };

  return (
    <Box sx={{ bgcolor: "black", width: "18%", padding: "2rem" }}>
      <Typography variant="h5" sx={{ fontWeight: "bold", color: "#ffffff" }}>
        <Link to={'/games'} className="home-head">Home</Link>
      </Typography>
      <Box>
        <Typography
          variant="h5"
          sx={{ fontWeight: "bold", color: "#ffffff", mt: "1rem" }}
        >
          Platforms
        </Typography>
        <List sx={{ display: "flex", flexDirection: "column" }}>
          {platforms.map((platform, index) => {
            const Icon = platform.icon;
            const isHovered = hoveredIndex === index;
            return (
              <Link key={index} to={`/games/platform/${platform.id}`} className="genres">
                <ListItem
                  sx={{ pt: "0", pb: "0", cursor: "pointer" }}
                  onMouseEnter={() => handleMouseEnter(index)}
                  onMouseLeave={handleMouseLeave}
                  onClick={() => getEachPlatform(platform.id)}
                >
                  <Box
                    sx={{
                      ...style,
                      backgroundColor: isHovered ? "#fff" : "#545454",
                    }}
                  >
                    <Icon
                      color={isHovered ? "black" : "#fff"}
                      fontSize="1.2rem"
                    />
                  </Box>
                  <Typography
                    variant="h6"
                    sx={{
                      color: "#fff",
                      p: "0.5rem",
                      fontWeight: "500",
                    }}
                  >
                    {platform.name}
                  </Typography>
                </ListItem>
              </Link>
            );
          })}
        </List>
      </Box>
      <Box>
        <Typography variant="h5" sx={{ fontWeight: "bold", color: "#ffffff" }}>
          Genres
        </Typography>
        <List sx={{ display: "flex", flexDirection: "column" }}>
          name
          {visibleItems.map((genre) => {
            const Image = genre.image_background;
            return (
              <Link
                to={`/games/genres/${genre.slug}`}
                className="genres"
                key={genre.id}
              >
                <ListItem sx={{ pt: "0", pb: "0", cursor: "pointer" }}>
                  <Box sx={{ width: "2rem", height: "2rem" }}>
                    <img
                      src={Image}
                      style={{
                        width: "2rem",
                        height: "2rem",
                        borderRadius: "0.3rem",
                      }}
                      alt={genre.name}
                    />
                  </Box>
                  <Typography
                    variant="h6"
                    sx={{
                      color: "#fff",
                      p: "0.5rem",
                      fontWeight: "500",
                    }}
                  >
                    {genre.name}
                  </Typography>
                </ListItem>
              </Link>
            );
          })}
          {!showAll && data.length > 4 && (
            <Button
              onClick={handleShowAll}
              color="primary"
              endIcon={<FaAngleDown />}
            >
              Show All
            </Button>
          )}
          {showAll && (
            <Button
              onClick={handleHide}
              color="primary"
              endIcon={<FaAngleUp />}
            >
              Hide
            </Button>
          )}
        </List>
      </Box>
    </Box>
  );
};

export default Sidebar;
