import axios from "axios";

const API_KEY = import.meta.env.VITE_API_KEY;


//generes 
export const getGenres= () => {
    const response = axios.get(
      `https://api.rawg.io/api/genres?token&key=${API_KEY}`
    );
    return response;
  };
  
//get games data 

export const getGames= (pagenumber,orderBy) => {
  console.log(orderBy)
  const response = axios.get(
    `https://rawg.io/api/games?token&key=${API_KEY}&page=${pagenumber}&ordering=-${orderBy}`
  );
  return response;
};

//get games by search

export const getSearchGames =(searchValue)=>{
  const response = axios.get(
      `https://api.rawg.io/api/games?search=${searchValue}?token&key=${API_KEY}&page_size=40`
  );
  return response
}

//get by genre

export const getByGenre = (genreSlug,orderBy)=>{
  const response = axios.get(
    `https://rawg.io/api/games?token&key=${API_KEY}&genres=${genreSlug}&page_size=40&ordering=-${orderBy}`
  )
  return response
}

export const getByPlatform =(id,orderBy)=>{
  const response = axios.get(
  `https://rawg.io/api/games/?token&key=${API_KEY}&parent_platforms=${id}&page_size=40&ordering=-${orderBy}`
  );
  return response;
}