import { configureStore } from "@reduxjs/toolkit";
import GenreSlice from './slices/GenereSlice';
import GameSlice from './slices/GameSlice';
import GenereDataSlice from './slices/GenereDataSlice';
import PlatformSlice from "./slices/PlatformSlice";
import orderBySlice from "./slices/OrderBySlice";

const store = configureStore({
     reducer:{
        genre:GenreSlice,
        game:GameSlice,
        genreData:GenereDataSlice,
        platformData:PlatformSlice,
        orderBy:orderBySlice
     }
})

export default store;
