import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  games: [],
  pageNumber: 1,
  isLoading: true,
  isError: null,
};

const GameSlice = createSlice({
  name: "Genre",
  initialState,
  reducers: {
    getGamesData(state, action) {
      state.games = action.payload;
    },
    getPageNumber(state, action) {
      state.pageNumber = action.payload;
    },
  },
});

export default GameSlice.reducer;

export const { getGamesData, getPageNumber } = GameSlice.actions;
