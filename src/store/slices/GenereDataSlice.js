import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  games: [],
  pageNumber: 1,
  isLoading: true,
  isError: null,
};

const GenereDataSlice = createSlice({
  name: "Genre",
  initialState,
  reducers: {
    genereData(state, action) {
      state.games = action.payload;
    },
    getPageNumber(state, action) {
      state.pageNumber = action.payload;
    },
  },
});

export default GenereDataSlice.reducer;

export const { genereData, getPageNumber } = GenereDataSlice.actions;
