import { createSlice } from "@reduxjs/toolkit";


const GenreSlice = createSlice({
    name:'Genre',
    initialState:[],
    reducers: {
        getGenresData(state,action){
           return state = action.payload
        }
    }
})

export default GenreSlice.reducer;

export const { getGenresData } = GenreSlice.actions;
