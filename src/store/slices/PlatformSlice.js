import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  games: [],
  pageNumber: 1,
  isLoading: true,
  isError: null,
};

const PlatformSlice = createSlice({
  name: "Genre",
  initialState,
  reducers: {
    platformData(state, action) {
      state.games = action.payload;
    },
    getPageNumber(state, action) {
      state.pageNumber = action.payload;
    },
  },
});

export default PlatformSlice.reducer;

export const { platformData, getPageNumber } = PlatformSlice.actions;
